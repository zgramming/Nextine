import { SidebarLayoutContext } from '@/context/SidebarLayoutContext';
import { MenuV2 } from '@/interface/category_modul';
import { isSideMenuActive } from '@/utils/function';
import { Stack, Collapse } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { IconUserBolt, IconChevronUp, IconChevronDown } from '@tabler/icons-react';
import { useRouter } from 'next/router';
import { useContext } from 'react';

type NavbarMenuItemProps = {
  item: MenuV2;
};

const NavbarMenuItem = ({ item }: NavbarMenuItemProps) => {
  const { toggleSidebar } = useContext(SidebarLayoutContext);
  const { push, pathname } = useRouter();
  const [opened, { toggle }] = useDisclosure(true);
  const subMenus = item.subMenus ?? [];
  const isHaveChild = subMenus.length > 0;

  const isActive = isSideMenuActive({
    currentPath: pathname,
    link: item.path ?? '',
  });

  const onClickMenu = () => {
    if (isHaveChild) {
      toggle();
    } else {
      const path = `${item.path}`;
      if (path) {
        toggleSidebar();
        push(`/${path}`);
      }
    }
  };

  return (
    <Stack gap={0}>
      <div
        key={item.id}
        className={`
          flex flex-row items-start gap-3 w-full cursor-pointer font-medium py-3 px-5 border-solid border-0
          ${isActive && 'text-primary bg-primary/20 border-r-4'}
          lg:px-8 lg:py-3
          hover:bg-primary/20 hover:text-primary
          `}
        onClick={onClickMenu}
      >
        <div className="grow basis-1/12">
          <IconUserBolt size={24} className="text-primary" />
        </div>
        {!isHaveChild && (
          <>
            <div className="grow basis-11/12 text-base">{item.name}</div>
          </>
        )}
        {isHaveChild && (
          <>
            <div className="grow basis-10/12 text-base">{item.name}</div>
            <div className="grow basis-1/12">
              {opened ? <IconChevronUp size={24} color="gray" /> : <IconChevronDown size={24} color="gray" />}
            </div>
          </>
        )}
      </div>
      <Collapse in={opened} transitionDuration={200} transitionTimingFunction="linear">
        <Stack gap={0}>
          {subMenus.map((subItem) => {
            return <NavbarSubMenuItem id={`${subItem.id}`} name={subItem.name} key={subItem.id} path={subItem.path} />;
          })}
        </Stack>
      </Collapse>
    </Stack>
  );
};

const NavbarSubMenuItem = ({ id, name, path }: { id: string; name: string; path?: string }) => {
  const { toggleSidebar } = useContext(SidebarLayoutContext);
  const { push, pathname } = useRouter();
  const isActive = isSideMenuActive({
    currentPath: pathname,
    link: path ?? '',
  });

  const onClickMenu = () => {
    if (path) {
      toggleSidebar();
      push(`/${path}`);
    }
  };
  return (
    <div
      key={id}
      className={`
        flex flex-row items-start gap-3 w-full cursor-pointer font-medium py-3 pl-14 border-solid border-0
        ${isActive && 'text-primary bg-primary/20 border-r-4'}
        lg:pl-12 lg:py-3 
        hover:bg-primary/20 hover:text-primary
        `}
      onClick={onClickMenu}
    >
      <div className="grow basis-1/12">
        <IconUserBolt size={24} className="text-primary" />
      </div>
      <div className="grow basis-10/12 text-base">{name}</div>
    </div>
  );
};

export default NavbarMenuItem;
